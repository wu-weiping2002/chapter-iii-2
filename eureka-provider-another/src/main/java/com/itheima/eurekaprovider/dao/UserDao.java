package com.itheima.eurekaprovider.dao;

import com.itheima.eurekaprovider.pojo.User;

import java.util.List;

public interface UserDao {
    public User findByUser(String username, String password);
    public User findByUser(User user);
    public List<User> findAll();
    public Integer add(User user);
    public Integer update(User user);
    public Integer delete(Integer id);
}
