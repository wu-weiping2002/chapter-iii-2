package com.itheima.eurekaprovider;

import com.itheima.eurekaprovider.dao.CustomerRepository;
import com.itheima.eurekaprovider.dao.UserDao;
import com.itheima.eurekaprovider.pojo.User;
import com.itheima.eurekaprovider.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EurekaProviderApplicationTests {
    @Autowired
    UserService userService;
    @Autowired
    UserDao userDao;

    @Autowired
    CustomerRepository customerRepository;

    @Test
    public void contextLoads() {
    }

    @Test
    public void userDaoTest(){
        System.out.println(userDao.findAll());
        System.out.println(userDao.findByUser("tom","111"));
    }
    @Test
    public void loginTest(){
        userService.login("Tom","123");
    }

    @Test
    public void customerTest(){
        System.out.println(customerRepository.findAll());
        System.out.println(customerRepository.findById(1));
        System.out.println(customerRepository.findByUsername("Jerry"));
    }
}
