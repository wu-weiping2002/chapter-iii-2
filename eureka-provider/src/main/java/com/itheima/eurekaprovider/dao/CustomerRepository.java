package com.itheima.eurekaprovider.dao;

import com.itheima.eurekaprovider.pojo.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
public interface CustomerRepository extends JpaRepository<Customer,Integer> {
    public Customer findByUsername(String username);
}
